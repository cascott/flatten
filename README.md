Flatten
=======

A tiny directory flatten tool. Use at your own risk :ghost:

This tool was created to flatten directories because at CA Scott we would get
many excel files in deeply nested directories. This allowed another excel vba
tool to merge cells and do things to it :grin:

This could have been done using a shell command but everyone was using 
windows and they where actuarial students.

Build Requirements
==================

Ok, Sadly I used IntelliJ to build the interface and the project
so you are going to need IntelliJ 11 or above to build this project.
The IntelliJ Community Edition will work fine.

You are also going to need Groovy to build the project.